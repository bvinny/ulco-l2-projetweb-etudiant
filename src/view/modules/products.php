
<div id="product">

    <div>

    <div class="product-images">
    <img id="img" src="/public/images/<?= $params["product"]["image" ] ?>" />
    <div class="product-miniatures">
    <div>
    <img id="img1" src="/public/images/<?= $params["product"]["image" ] ?>" />
    </div>
    <div>
    <img id="img2" src="/public/images/<?= $params["product"]["image_alt1" ] ?>"/>
    </div>
        <div>
            <img id="img3" src="/public/images/<?= $params["product"]["image_alt2" ] ?>"/>
        </div>
        <div>
            <img id="img4" src="/public/images/<?= $params["product"]["image_alt3" ] ?>"/>
        </div>
    </div>
    </div>
    <div class="product-infos">
    <p class="product-category">
    <?= $params["product"]["nomCategorie" ] ?>
    </p>
    <h1>
    <?= $params["product"]["name" ] ?>
    </h1>
    <p class="product-price">
    <?= $params["product"]["price" ] ?>
    </p>
    <form>
        <button id="btn1" type="button">
        -
        </button>
        <button id="btn2" type="button">
        1
        </button>
        <button id="btn3" type="button">
        +
        </button>
        <input type="submit" value="Ajouter dans le panier">

    </form>
    </div>
    </div>

    <div>
    <div class="product-spec">
    <h2>
        Spécificités
    </h2>
    <?= $params["product"]["spec" ] ?>
    </div>
    <div class="product-comments">
        <h2>
            avis
        </h2>
        <p>
            Il n'y a pas d'avis pour ce produit.
        </p>
    </div>
    </div>
    </div>
</div>
<script src="/public/scripts/product.js"></script>