<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function listProduits(): array
  {
      // connexion à la base de données
      $db = \model\Model::connect();

      // Requete SQL
      $sql = " SELECT product.id, product.name, product.price, product.image, category.name as nomCategorie FROM product INNER JOIN category on product.category = category.id";

      //Exécution de la requete
      $req = $db->prepare($sql);
      $req->execute();

      //Retourner les resultats (type array)
      return $req->fetchAll();
  }

  static function infoProduct(int $id)
  {
      // connexion à la base de données
      $db = \model\Model::connect();

      // Requete SQL
      $sql = " SELECT product.name, product.price,product.image_alt1, product.image_alt2, product.image_alt3,  product.image, product.spec, category.name as nomCategorie FROM product INNER JOIN category ON (product.category = category.id) WHERE product.id=$id";

      //Exécution de la requete
      $req = $db->prepare($sql);
      $req->execute();

      //Retourner les resultats (type array)
      return $req->fetch();
  }
}