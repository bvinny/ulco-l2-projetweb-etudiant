<?php


namespace controller;


class AccountController
{
    public function account(): void
    {
        $params = array(
            "title" => "account",
            "module" => "account.php"
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    public function login(): void
    {
        $mail =htmlspecialchars($_POST['usermail']);
        $password = htmlspecialchars($_POST['userpass']);
        $psw = password_hash("$password", PASSWORD_DEFAULT);
        $login= \model\AccountModel::login($mail, $psw);
    }

    public function signin(): void
    {
        $lastname=htmlspecialchars($_POST['userlastname']);
        $firstname=htmlspecialchars($_POST['userfirstname']);
        $mail=htmlspecialchars($_POST['usermail']);
        $password=htmlspecialchars($_POST['userpass']);
        $mdp = password_hash("$password", PASSWORD_DEFAULT);
        $signin= \model\AccountModel::signin($firstname, $lastname, $mail, $mdp);

        // Faire la redirection "src/view/Template.php"
        header("location: /account");
    }
}