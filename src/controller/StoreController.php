<?php

namespace controller;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $list= \model\StoreModel::listProduits();
    // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
        "list" => $list
    );

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);

  }

    public function product(int $id): void
    {
        // Attention, ta méthode infoProduct prend un paramètre !
        // Et nous retourne les informations du produit dans $product
        $product = \model\StoreModel::infoProduct($id);

        //si le produit n'existe pas
   if($product == null){
       header("location: /store");
       exit();


   }
        $params = array (

            "title"   => "Produit",
            "module"  => "products.php",
            "product" => $product // les informations du produit, OK
        );

        // Enfin, on souhaite faire le rendu du Template avec les paramètres d'au dessus
        // ça ne change pas
        \view\Template::render($params);
  // Variables à transmettre au template

}
}